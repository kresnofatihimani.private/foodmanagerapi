package tech.kresnofatih.foodmanager;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.kresnofatih.foodmanager.models.AppUser;
import tech.kresnofatih.foodmanager.services.AppUserService;

import java.util.Map;

@RestController
@RequestMapping("/appuser")
public class AppUserResource {
    private final AppUserService appUserService;

    public AppUserResource(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    @GetMapping("/findAll")
    public ResponseEntity<Map<Long, AppUser>> getAllUsers()
    {
        Map<Long, AppUser> appUsers = appUserService.findAllAppUsers();
        return new ResponseEntity<>(appUsers, HttpStatus.OK);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<AppUser> getAppUserById(@PathVariable("id") Long id)
    {
        AppUser appUser = appUserService.findAppUserById(id);
        return new ResponseEntity<>(appUser, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<AppUser> addNewUser(@RequestBody AppUser appUser)
    {
        AppUser newUser = appUserService.addAppUser(appUser);
        return new ResponseEntity<>(newUser, HttpStatus.OK);
    }
}
