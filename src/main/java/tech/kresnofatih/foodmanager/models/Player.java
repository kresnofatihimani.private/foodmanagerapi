package tech.kresnofatih.foodmanager.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class Player implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID id;

    @Column(nullable = false)
    private String name;
    private Long points;
    private String imageUrl;

    public Player() {}

    public Player(UUID id, String name, Long points, String imageUrl)
    {
        this.id = id;
        this.name = name;
        this.points = points;
        this.imageUrl = imageUrl;
    }

    // getters
    public UUID getId()
    {
        return id;
    }

    @JsonIgnore
    public String getIdAsString()
    {
        return id.toString();
    }

    public String getName()
    {
        return name;
    }

    public Long getPoints()
    {
        return points;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }

    // setters
    public void setId(UUID id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setPoints(Long points)
    {
        this.points = points;
    }

    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
    }
}
