package tech.kresnofatih.foodmanager.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

@Entity
public class AppUser implements Serializable, UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    private boolean isNonExpired;

    private boolean isNonLocked;

    private boolean isCredentialsNonExpired;

    private boolean isEnabled;

    @Column(nullable = false)
    private String role;

    public AppUser(){}

    public AppUser(Long id, String username, String password, boolean isNonExpired, boolean isNonLocked, boolean isCredentialsNonExpired, boolean isEnabled, String role)
    {
        this.id = id;
        this.username = username;
        this.password = password;
        this.isNonExpired = isNonExpired;
        this.isNonLocked = isNonLocked;
        this.isCredentialsNonExpired = isCredentialsNonExpired;
        this.isEnabled = isEnabled;
        this.role = role;
    }

    // Getter
    public Long getId()
    {
        return id;
    }

    @JsonIgnore
    public String getIdAsString()
    {
        return id.toString();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.asList(new SimpleGrantedAuthority(role));
    }

    @Override
    public String getPassword()
    {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    @JsonIgnore
    public boolean getIsNonExpired()
    {
        return isNonExpired;
    }

    @JsonIgnore
    public boolean getIsNonLocked()
    {
        return isNonLocked;
    }

    @JsonIgnore
    public boolean getIsCredentialsNonExpired()
    {
        return isCredentialsNonExpired;
    }

    @JsonIgnore
    public boolean getIsEnabled()
    {
        return isEnabled;
    }

//    @JsonIgnore
    public String getRole()
    {
        return role;
    }

    // setter
    public void setId(Long id)
    {
        this.id = id;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public void setIsNonExpired(boolean isNonExpired)
    {
        this.isNonExpired = isNonExpired;
    }

    public void setIsNonLocked(boolean isNonLocked)
    {
        this.isNonLocked = isNonLocked;
    }

    public void setIsCredentialsNonExpired(boolean isCredentialsNonExpired)
    {
        this.isCredentialsNonExpired = isCredentialsNonExpired;
    }

    public void setIsEnabled(boolean isEnabled)
    {
        this.isEnabled = isEnabled;
    }

    public void setRole(String role)
    {
        this.role = role;
    }
}
