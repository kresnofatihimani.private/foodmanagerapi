package tech.kresnofatih.foodmanager.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Food implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false)
    private String name;
    private Long calories;
    private String imageUrl;

    public Food() {}

    public Food(Long id, String name, Long calories, String imageUrl)
    {
        this.id = id;
        this.name = name;
        this.calories = calories;
        this.imageUrl = imageUrl;
    }

    // getters
    public Long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public Long getCalories()
    {
        return calories;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }

    // setters
    public void setId(Long id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setCalories(Long calories)
    {
        this.calories = calories;
    }

    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString()
    {
        return "Food{"      +
                "id="       +   id      +   "|" +
                "name="     +   name    +   "|" +
                "calories"  +   calories+   "|" +
                "imageUrl"  +   imageUrl+
                "}";
    }
}
