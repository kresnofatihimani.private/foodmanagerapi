package tech.kresnofatih.foodmanager.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.kresnofatih.foodmanager.exceptions.PlayerNotFoundException;
import tech.kresnofatih.foodmanager.models.Player;
import tech.kresnofatih.foodmanager.repository.PlayerRepository;

import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class PlayerService {
    private final PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public Player addPlayer(Player player)
    {
        return playerRepository.save(player);
    }

    public Map<String, Player> findAllPlayers()
    {
        return playerRepository
                .findAll()
                .stream()
                .collect(Collectors.toMap(Player::getIdAsString, Function.identity()));
    }

    public Player findPlayerById(UUID id)
    {
        return playerRepository
                .findPlayerById(id)
                .orElseThrow(()->new PlayerNotFoundException(id));
    }
}
