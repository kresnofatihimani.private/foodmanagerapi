package tech.kresnofatih.foodmanager.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.kresnofatih.foodmanager.exceptions.FoodNotFoundException;
import tech.kresnofatih.foodmanager.models.Food;
import tech.kresnofatih.foodmanager.repository.FoodRepository;

import java.util.List;
import java.util.Random;

@Service
public class FoodService {
    private final FoodRepository foodRepository;

    @Autowired
    public FoodService(FoodRepository foodRepository) {
        this.foodRepository = foodRepository;
    }

    public Food addFood(Food food)
    {
        Random rand = new Random();
        int upperBound = 350;
        Long randomCalories = (long) rand.nextInt(upperBound);
        food.setCalories(randomCalories);
        return foodRepository.save(food);
    }

    public List<Food> findAllFoods()
    {
        return foodRepository.findAll();
    }

    public Food updateFood(Food food)
    {
        return foodRepository.save(food);
    }

    public Food findFoodById(Long id)
    {
        return foodRepository
                .findFoodById(id)
                .orElseThrow(()->new FoodNotFoundException(id));
    }

    public void deleteFood(Long id)
    {
        foodRepository.deleteFoodById(id);
    }
}
