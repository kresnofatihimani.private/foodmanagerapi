package tech.kresnofatih.foodmanager.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.kresnofatih.foodmanager.exceptions.UserNotFoundException;
import tech.kresnofatih.foodmanager.models.AppUser;
import tech.kresnofatih.foodmanager.repository.AppUserRepository;

import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class AppUserService {
    private final AppUserRepository appUserRepository;

    @Autowired
    public AppUserService(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    public AppUser addAppUser(AppUser appUser)
    {
        return appUserRepository.save(appUser);
    }

    public Map<Long, AppUser> findAllAppUsers()
    {
        return appUserRepository
                .findAll()
                .stream()
                .collect(Collectors.toMap(AppUser::getId, Function.identity()));
    }

    public AppUser findAppUserById(Long id)
    {
        return appUserRepository
                .findAppUserById(id)
                .orElseThrow(()->new UserNotFoundException(id));
    }

    public AppUser findAppUserByUsername(String username)
    {
        return appUserRepository
                .findByUsername(username)
                .orElse(null);
    }

    public String findAppUserRole(String username)
    {
        AppUser foundUser = findAppUserByUsername(username);
        if(!Objects.isNull(foundUser))
        {
            return foundUser.getRole();
        }
        return null;
    }
}
