package tech.kresnofatih.foodmanager.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.stereotype.Service;
import tech.kresnofatih.foodmanager.utility.DateTimeUtility;

@Service
public class JwtService {
    private final String secret = "x3zz5JnGYjUwAuA33CxeQW7mnH2vRK";

    private final Algorithm signAlgorithm = Algorithm.HMAC256(secret);

    public String createToken(Long id, String username, String role)
    {
        try {
            DateTimeUtility dateTimeUtility = new DateTimeUtility();
            return JWT
                    .create()
                    .withClaim("uid", id)
                    .withClaim("usn", username)
                    .withClaim("rle", role)
                    .withExpiresAt(dateTimeUtility.getDateAddMinutesFromNow(3))
                    .sign(signAlgorithm);
        } catch (JWTCreationException exception)
        {
            return null;
        }
    }

    public boolean validateToken(String token)
    {
        try {
            JWTVerifier verifier = JWT
                    .require(signAlgorithm)
                    .build();
            DecodedJWT jwt = verifier.verify(token);
            return true;
        } catch (JWTVerificationException exception)
        {
            return false;
        }
    }

    public String extractClaim(String token, String claimType)
    {
        DecodedJWT jwt = decodeToken(token);
        return jwt.getClaim(claimType).toString().replaceAll("^\"|\"$", "");
    }

    private DecodedJWT decodeToken(String token)
    {
        try {
            return JWT.decode(token);
        } catch (JWTDecodeException exception)
        {
            throw new RuntimeException(exception);
        }
    }

}
