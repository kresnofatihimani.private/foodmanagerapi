package tech.kresnofatih.foodmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.kresnofatih.foodmanager.models.Player;

import java.util.Optional;
import java.util.UUID;

public interface PlayerRepository extends JpaRepository<Player, String> {

    Optional<Player> findPlayerById(UUID id);
}
