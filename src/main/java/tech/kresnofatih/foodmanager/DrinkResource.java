package tech.kresnofatih.foodmanager;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.kresnofatih.foodmanager.cache.service.DrinkCacheService;
import tech.kresnofatih.foodmanager.models.Drink;
import tech.kresnofatih.foodmanager.services.DrinkService;

import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/drink")
public class DrinkResource {
    private final DrinkService drinkService;

    private final DrinkCacheService drinkCacheService;

    public DrinkResource(DrinkService drinkService, DrinkCacheService drinkCacheService) {
        this.drinkService = drinkService;
        this.drinkCacheService = drinkCacheService;
    }

    @GetMapping("/findAll")
    public ResponseEntity<Map<String, Drink>> getAllDrinks()
    {
        Map<String, Drink> drinks = drinkService.findAllDrinks();
        return new ResponseEntity<>(drinks, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Drink> getDrinkById(@PathVariable("id") Long id)
    {
        Drink drink = drinkService.findDrinkById(id);
        return new ResponseEntity<>(drink, HttpStatus.OK);
    }

    @GetMapping("/findIf/{id}")
    public ResponseEntity<Drink> getDrinkIfById(@PathVariable("id") Long id)
    {
        Drink drinkCache = drinkCacheService.findById(id);
        Drink res = drinkCache;
        if(Objects.isNull(drinkCache)) {
            System.out.println("finding in db");
            res = drinkService.findDrinkById(id);
            drinkCacheService.save(res);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping("/findWithQuery")
    public ResponseEntity<Drink> getDrinkByName(@RequestParam String name)
    {
        Drink drink = drinkService.findDrinkByName(name);
        return new ResponseEntity<>(drink, HttpStatus.OK);
    }

    @GetMapping("/findWithCaloriesCriteria")
    public ResponseEntity<Map<String, Drink>> getDrinkWithCaloriesCriteria(@RequestParam String criteria, @RequestParam Long caloriesLimit)
    {
        Map<String, Drink> drinks = drinkService.findDrinkWithCaloriesCriteria(caloriesLimit, criteria);
        return new ResponseEntity<>(drinks, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Drink> addDrink(@RequestBody Drink drink)
    {
        Drink newDrink = drinkService.addDrink(drink);
        return new ResponseEntity<>(newDrink, HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Drink> updateDrink(@RequestBody Drink drink, @PathVariable("id") Long id)
    {
        Drink updated = drinkService.updateDrink(drink, id);
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteDrink(@PathVariable("id") Long id)
    {
        drinkService.deleteDrink(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping("/cache/add")
    public ResponseEntity<Drink> addDrinkCache(@RequestBody Drink drink)
    {
        drinkCacheService.save(drink);
        return new ResponseEntity<>(drink, HttpStatus.OK);
    }

    @GetMapping("cache/findById/{id}")
    public ResponseEntity<Drink> findDrinkCacheById(@PathVariable("id") Long id)
    {
        Drink foundDrink = drinkCacheService.findById(id);
        return new ResponseEntity<>(foundDrink, HttpStatus.OK);
    }

    @DeleteMapping("cache/deleteById/{id}")
    public ResponseEntity<Boolean> deleteById(@PathVariable("id") Long id)
    {
        Drink foundDrink = drinkCacheService.findById(id);
        drinkCacheService.delete(foundDrink);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
}
