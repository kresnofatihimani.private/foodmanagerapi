package tech.kresnofatih.foodmanager.cache.repository;

import java.util.List;

public interface BaseCacheRepository<ID, T> {
    List<T> findAll();

    T findById(ID id);

    void save(T entity);

    void delete(T entity);
}
