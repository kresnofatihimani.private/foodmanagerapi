package tech.kresnofatih.foodmanager.cache.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import tech.kresnofatih.foodmanager.cache.repository.BaseCacheRepository;
import tech.kresnofatih.foodmanager.models.Drink;

import java.util.List;

@Service
public class DrinkCacheService implements BaseCacheRepository<Long, Drink> {

    private final RedisTemplate<Long, Object> redisTemplate;

    @Autowired
    public DrinkCacheService(RedisTemplate<Long, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public List<Drink> findAll() {
        return null;
    }

    @Override
    public Drink findById(Long id) {
        return (Drink) redisTemplate.opsForValue().get(id);
    }

    @Override
    public void save(Drink drink) {
        redisTemplate.opsForValue().set(drink.getId(), drink);
    }

    @Override
    public void delete(Drink drink) {
        redisTemplate.delete(drink.getId());
    }
}
