package tech.kresnofatih.foodmanager.exceptions;

import java.util.UUID;

public class PlayerNotFoundException extends RuntimeException {
    public PlayerNotFoundException(UUID id) {
        super(
                "Player by id = " + id.toString() + " is not found"
        );
    }
}
