package tech.kresnofatih.foodmanager.exceptions;

public class DrinkNotFoundException extends RuntimeException{
    public DrinkNotFoundException(Long id) {
        super(
                "Drink by id = " + id.toString() + " is not found"
        );
    }

    public DrinkNotFoundException(String name){
        super(
                "Drink by name: " + name + " is not found"
        );
    }

    public DrinkNotFoundException(String criteria, Long numericLimit){
        super(
                "Drink by criteria: " + criteria + " "
                        + numericLimit.toString() + " is not found"
        );
    }
}
