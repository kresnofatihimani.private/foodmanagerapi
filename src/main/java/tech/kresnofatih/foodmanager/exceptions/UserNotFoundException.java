package tech.kresnofatih.foodmanager.exceptions;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(Long id) {
        super(
                "User by id " + id.toString() + " is not found"
        );
    }

    public UserNotFoundException(String username)
    {
        super(
                "User by username " + username + " is not found"
        );
    }
}
