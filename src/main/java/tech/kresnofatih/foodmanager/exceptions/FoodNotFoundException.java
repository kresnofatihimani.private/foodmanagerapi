package tech.kresnofatih.foodmanager.exceptions;

public class FoodNotFoundException extends RuntimeException{
    public FoodNotFoundException(Long id) {
        super(
                "Food by id =" + id.toString() + " is not found"
        );
    }
}
