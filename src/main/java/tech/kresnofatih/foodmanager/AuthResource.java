package tech.kresnofatih.foodmanager;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.kresnofatih.foodmanager.security.AuthRequest;
import tech.kresnofatih.foodmanager.security.AuthResponse;
import tech.kresnofatih.foodmanager.services.AuthService;
import tech.kresnofatih.foodmanager.services.JwtService;

@RestController
@RequestMapping("/auth")
public class AuthResource {
    private final AuthService authService;

    private final JwtService jwtService;

    public AuthResource(AuthService authService, JwtService jwtService) {
        this.authService = authService;
        this.jwtService = jwtService;
    }

    @PostMapping("/login")
    public ResponseEntity<AuthResponse> getToken(@RequestBody AuthRequest authRequest)
    {
        AuthResponse resp = authService.getToken(authRequest);
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

    @GetMapping("/extractId")
    public ResponseEntity<String> getRole(@RequestParam("tkn") String token)
    {
        String resp = jwtService.extractClaim(token, "uid");
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }
}
